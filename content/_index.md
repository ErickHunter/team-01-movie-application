# Movie-API
 
[![Build Status](https://gitlab.cs.ttu.ee/antran/movie-application/badges/master/pipeline.svg)](https://gitlab.cs.ttu.ee/antran/movie-application/-/pipelines)
# Component Diagram

![Component](https://i.ibb.co/3fFCddB/Component-Diagram.png)

## Description:

The gitlab-ci runner is installed in my local machine inside wsl2 and the production is an AWS ec2 instance. both are ubuntu 20.04. Both machines are configured by hand. Gitlab runner is used for building, testing, deploying the new app to the production server by ssh. shell is used as an executor for the gitlab runner. The build stage inside the gitlab runner creates a Jar file which has the backend API then the gitlab runner copies it to the production server. The production server has docker-compose yaml file that creates two containers, one has java and copies the jar file and runs it with exposing the endpoint to the other container and the other container has nginx proxy server with getting the ssl certificates from the host machine along with the configuration file. this docker container expose to both ports of http and https and accepts the requests coming to the ec2 instance. I used noip for dynamic dns with making the IPv4 of the ec2 with port 443 available anywhere.
 
# Initial setup
 
 Get an EC2 instance and connect via ssh.
 We are using ubuntu 20.04 machines.
 I put my public keys in authorized_keys in .ssh
 
### Install Git lab runner
gitlab runner was installed on our local machine using a deb package as shown [here](https://docs.gitlab.com/runner/install/linux-manually.html).
 
Then runner is registered on the machine as shown [here](https://docs.gitlab.com/runner/register/) and .gitlab-ci.yml is placed in the project repo.

.gitlab-ci.yml

```yml
stages:
  - build
  - test
  - deploy

before_script:
  - export GRADLE_USER_HOME=`pwd`/.gradle

build api:
  stage: build
  cache:
    paths:
      - .gradle/wrapper
      - .gradle/caches
  artifacts:
    paths:
      - build/libs
  tags:
    - api-home
  script:
    - ./gradlew assemble

test api:
  stage: test
  tags:
    - api-home
  script:
    - ./gradlew check

deploy:
  stage: deploy
  only:
    refs:
      - main
  tags:
    - api-home
  script:
    - scp -r build/libs/ gitlab-runner@ec2-18-195-198-65.eu-central-1.compute.amazonaws.com:~/api
    - ssh ubuntu@ec2-18-195-198-65.eu-central-1.compute.amazonaws.com 'cd ~/api;sudo docker-compose down;sudo docker-compose up -d'
```
 
The executor chosen was shell and the tag of the local machine's gitlab runner is 'api-home'
In order to connect the gitlab server and the production server, ssh keys were generated for gitlab-runner user on and the public key of the api-home was placed into authorized_keys of the production server .ssh folder. This allowed for the jar file from api-home to be copied to the ec2 instance via scp and send commands via ssh in order to start docker-compose.
 
### Installing dependencies
- Docker and Docker-compose are installed on the ec2 instance following the docker documentation.
[Compose/install](https://docs.docker.com/compose/install/)
[Engine/install](https://docs.docker.com/engine/install/ubuntu/)
 
 
- Open jdk 11 is installed on the local machine by running the following command.
   > sudo apt-get install openjdk-11-jre openjdk-11-jdk
 
## Docker file
Inside the instance a dockerfile was created that copies the jar file inside the container and runs it.
 
## Nginx
Installed nginx server and certbot on the ec2 instance 'api'.
 
nginx
> sudo apt install nginx
 
Certbot was used to allow for use of a letsencrypt certificate.
This was setup according to certbot documentation witch can be found at [cerbot.eff.org](https://certbot.eff.org/lets-encrypt/ubuntufocal-nginx)
 
## App configuration
app.conf was added to /etc/nginx/conf.d/
 
```conf
server {
server_name movie-api.ddns.net;
 
location / {
  proxy_pass http://localhost:8081;
  proxy_http_version 1.1;
  proxy_set_header Upgrade $http_upgrade;
  proxy_set_header Connection 'upgrade';
  proxy_set_header Host $host;
  proxy_cache_bypass $http_upgrade;
 }
 
   listen 443 ssl; # managed by Certbot
   ssl_certificate /etc/letsencrypt/live/movie-api.ddns.net/fullchain.pem; # managed by Certbot
   ssl_certificate_key /etc/letsencrypt/live/movie-api.ddns.net/privkey.pem; # managed by Certbot
   include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
   ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
 
}
server {
   if ($host = movie-api.ddns.net) {
       return 301 https://$host$request_uri;
   } # managed by Certbot
 
 
listen 80;
server_name movie-api.ddns.net;
   return 404; # managed by Certbot
}
 
```
 
Then the following command was issued,
> sudo certbot --nginx
 
A url was chosen and the nginx service was stopped with
> sudo service nginx stop
 
## docker compose
 
Docker-compose.yml
```yml
version: "3"
services:
 app:
   build:
     context: .
     dockerfile: Dockerfile
   restart: always
   tty: true
   networks:
     - app-network
   expose:
     - "8081"
 
 webserver:
   image: nginx:alpine
   container_name: webserver
   restart: always
   tty: true
   ports:
     - "80:80"
     - "443:443"
   volumes:
     - ./app.conf:/etc/nginx/conf.d/app.conf
     - ./nginx/error.log:/etc/nginx/error_log.log
     - /etc/letsencrypt/:/etc/letsencrypt/
   networks:
     - app-network
 
networks:
 app-network:
   driver: bridge
 
```
 in order to make the nginx server inside the docker container recognize the Java API I changed the proxy server in the api.conf configuration file

 ```conf
 location / {
   proxy_pass http://app:8081;
   proxy_http_version 1.1;
   proxy_set_header Upgrade $http_upgrade;
   proxy_set_header Connection upgrade'';
   proxy_set_header Host $host;
   proxy_cache_bypass $http_upgrade;
   }

 ```
## DNS
 
This is done one the AWS console.
We went to security groups of the instance and then set an inbound rule such that TCP port 443 is available anywhere.
 
We then got a subdomain from noip and linked it to our public ip address.

## Business analysis

Users may want to know what is the current trending movie but may not necessary want to deal with actually finding it themselves, Because of this Team_1 has developed the Movie-API using Amazon AWS..
The api has capability to easily find what movie has been trending in either the last day of week based on historical rating of the movie. 
 
 


